<script>
    $(document).ready(function () {
        $('#contactform').validate({ // initialize the plugin
            rules: {
                yourname: {
                    required: true,
                    minlength: 5
                },
                jobtitle: {
                    required: true,
                    minlength: 5
                },
                youremail: {
                    required: true,
                    email: true
                },
                yourtelephone: {
                    required: true,
                    minlength: 5
                },
                youraddress: {
                    required: true,
                    minlength: 5
                },
                message: {
                    required: true,
                    minlength: 5
                }
            },
            submitHandler: function (form) {
                form.submit();
                return false;
            }
        });
    });
</script>

    <footer id="involved">
        <div class="bottomfoot">
            <div class="container">
                <section class="info">
                    <h3 class="mainheader">Let's talk</h3>
                    <h3>+44 (0) 121 236 1988</h3>
                    <strong>Email</strong><br>
                    <strong>Professor Dilip Nathwani (NHS TAYSIDE)</strong><br>
                    <strong>Programme Director for the MOOC and BSAC President Elect</strong>
                    <h3 style="text-transform:lowercase;">dilip.nathwani@nhs.net</h3>

                    <strong>Tracey Guise</strong><br>
                    <strong>Chief Executive Officer - </strong>
                    <strong>British Society for Antimicrobial Chemotherapy</strong>
                    <h3 style="text-transform:lowercase;">TGuise@bsac.org.uk</h3>
                </section>
            </div>
        </div>
        <div class="registerbox" id="contact">
            <div class="container">
            <h3>Next steps - Register your interest</h3>
            <p>Contact us today</p>
            <div class="contactform registerform">
                <div class="">
                    <form id="contactform" method="post" action="thanks.php">
                        <ul>
                            <li class="first">
                                <label>Your name</label>
                                <span class="your-name">
                                    <input type="text" name="yourname" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>

                            <li>
                                <label>Job Title</label>
                                <span class="your-email">
                                    <input type="text" name="jobtitle" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>

                        
                            <li>
                                <label>Your email address</label>
                                <span class="your-email">
                                    <input type="email" name="youremail" value="" size="40" class="uniform-input email error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>
                        
                            <li>
                                <label>Telephone number</label>
                                <span class="your-telephone">
                                    <input type="text" id="yourtelephone" name="yourtelephone" value="" size="40" class="uniform-input text error" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>
                                                    
                            <li>
                                <label>Address</label>
                                <span class="your-address">
                                    <textarea name="youraddress" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                            <li>
                                <label>Comments</label>
                                <span class="your-address">
                                    <textarea name="message" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                        </ul>
                        
                        <input type="submit" value="Send">
                        
                    </form>
                </div>
            </div>
        </div>
        
        </div>
        <div class="bottomfoot">
            <div class="container">
                <section class="info">
                    <p><img style="margin-top:20px;" src="assets/img/bsac-logo-white-text.png" alt="BSAC logo"><br>
                    ADDRESS: 53 Regent Place | Birmingham | B1 3NJ</p>
                    <p>© BSAC 2014.</p>
                </section>
            </div>
        </div>
    </footer>

    <div id="back-top">
        <a href="#top"><span></span>Top</a>
    </div>

    
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $("#back-top").click(function(){
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    </script>

</body>
</html>