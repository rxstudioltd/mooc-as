<!DOCTYPE html>
<!-- saved from url=(0038)http://www.plugandplayfranchise.co.uk/ -->
<html lang="en-US" prefix="og: http://ogp.me/ns#" class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" style="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" media="all" href="assets/css/style.css">
    <script src="assets/js/modernizr-2.6.2.min.js"></script>
    <!--IE10 mediaquery hack -->
    <script type="text/javascript">
    function win8responsive() {
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
            document.createTextNode(
                "@-ms-viewport{width:auto!important}"
            )
        );
        document.getElementsByTagName("head")[0].
            appendChild(msViewportStyle)} }
    win8responsive();
    </script>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/site-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/site-icon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/img/site-icon.png">
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="apple-touch-startup-image" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Antimicrobial Stewardship - Massive Open Online Course</title>

    <meta name="robots" content="noodp,noydir">
    <meta name="description" content="">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Rx Guidelines">
    <meta property="og:description" content="">
    <meta property="og:url" content="http://www.rx-guidelines.com/">
    <meta property="og:site_name" content="Rx Guidelines Website">

    <link rel="stylesheet" id="contact-form-7-css" href="assets/css/styles.css" type="text/css" media="all">
    <script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
    <script src="assets/js/jquery.validate.js"></script>

    <script>
      $(function() {
        $('.mainmenu a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
              || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top-65
              }, 1000);
              return false;
            }
          }
        });
      });
      $(document).ready(function(){

            // hide #back-top first
            $("#back-top").hide();
            
            // fade in #back-top
            $(function () {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 100) {
                        $('#back-top').fadeIn();
                    } else {
                        $('#back-top').fadeOut();
                    }
                });

                // scroll body to 0px on click
                $('#back-top a').click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
            });

        });
    </script>

    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    
</head>

<body class="home page page-id-15 page-template page-template-frontpage-php" id="top">
	
    <header class="mainnav">
        <div class="container">
            <img class="logo"src="assets/img/logo.png" alt="Pharma Mix Group"> 

            <a href="index.html" id="menubtn"><span class="open">Main Menu</span><span class="close">Close</span></a>

            <nav>
                <ul id="menu-primary" class="mainmenu">
                    <li id="menu-item-11" class="first"><a href="#top">Home</a></li>
                   
                    <li id="menu-item-11" class=""><a href="#about">About</a></li>

                    <li id="menu-item-11" class=""><a href="#faculty">Faculty</a></li>

                    <li id="menu-item-11" class=""><a href="#partners">Partners</a></li>

                    <li id="menu-item-11" class=""><a href="#news">News</a></li>

                    <li id="menu-item-11" class=""><a href="#involved">Get Involved</a></li>

                    <li id="menu-item-11" class=""><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </div>   
    </header>
    
    <div class="clear"></div>
