<?php include ('header.php'); ?>

<style>
.mainnav ul .home-active a {
  background:#e74c2e !important;
  color:#fff;
  text-shadow:0 1px 1px rgba(0,0,0,0.5);
}
</style>

	<div id="body-container">
		<section class="home-intro">
            <div class="container">
                <h1>RX Guidelines</h1>
                <h2>Antibiotic stewardship at your fingertips</h2> 
                <h1>Mobile access to local antimicrobial prescribing guidelines via iphone and android devices.</h1>
			</div>
		</section>

        <section class="home-darkbox">
            <article class="panel leftside">
                <!--<img src="assets/img/clinician.png" alt="Clinicians icon">-->
                <div class="info">
                    <h3 class="mainheader">What is it?</h3>
                    <p>RxGuidelines seeks to improve the ease with which antimicrobial guidelines are accessible to healthcare professionals in local Trusts and enable good governance across the organisation. 
                    Developed by an expert faculty across Europe this application will be an essential tool for education and support of junior doctors within local Trusts to enable compliance to local guidelines with respect to antibiotic choice, length of treatment and other governance recommendations.
                    The application is available across IoS and Android platforms giving accessibility and usage to a wide number of devices.
                    “There is high level of smartphone ownership and usage among medical students and junior doctors. Both groups endorse the development of more apps to support their education and clinical practice” <sup>1</sup>.
                    <p>&nbsp;</p>
                    <div class="reference-text"><small>References<br>1. Smartphone and medical related App use among medical students and junior doctors in the United Kingdom (UK): a regional survey. Karl Frederick Braekkan Payne<sup>1</sup>, Heather Wharrad<sup>2*</sup> and Kim Watts<sup>2</sup> BMC Medical Informatics and decision making</small></div>
                </div>
            </article>

            <article class="panel rightside">
                <!--<img src="assets/img/clinician.png" alt="IT icon">-->
                <div class="info">
                    <h3 class="mainheader">How does it work?</h3>
                    <p>As a Trust, for a very competitive license fee you will be set up as a new user within the RxGuidelines system. This will enable you to input your local guidelines into a bespoke web application that will give you the ability to customise to your local needs.
                    Once you are happy with the guidelines format, the publish feature allows you to push the data to your local users.
                    RxGuidelines can be downloaded instantly for free to any iPhone/Android device and provide the user with simple and clear access to local Trust guidelines. 
                    Guidelines are managed through the web application and, when changes have occurred, these are “published” to the application so that users will always have the most current data that has been authorised by the main user(s) within the Trust.
                    Other tools within the application allow Trusts to publish guidelines relating to specific drugs and more general information regarding antibiotic stewardship.
                    This is a simple, cost effective solution for managing antimicrobial stewardship on a local basis.</p>
                </div>
            </article>
        </section>

        <section class="home-further">
            <div class="container">
                <h3>Application features</h3>
                <p>All healthcare professionals across the Trust need to adopt good antimicrobial stewardship principles. RxGuidelines brings all antimicrobial guidelines into one central source to enable the ease of access to all relevant information.</p>
                <p>This application, which is FREE to download, will be an essential part of clinical practice across the UK and has been developed by an expert faculty from across Europe to ensure relevance to local needs.</p>
                <p>A useful antimicrobial encyclopaedia is also included to enable Trusts to post specific information relating to drugs.</p>
            </div>
        </section>

        <section class="benefits">
            <div class="container">
                <h3>Extra features</h3>
                <div class="panel">
                    <img width="160" height="160" src="assets/img/ease_of_use.png" alt="benefit-leads">
                    <h4>Personalise your App</h4>
                    <p>list your most used Guidelines by order of preference. </p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/speed.png" alt="benefit-marketing">
                    <h4>quick access</h4>
                    <p>between guidelines via hyperlinks.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/encyclopaedia.png" alt="benefit-support">  
                    <h4>Antibiotic encyclopaedia</h4>
                    <p>local access easily and quickly.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/analytics.png" alt="benefit-crm">          
                    <h4>Storage</h4>
                    <p>optimised to take up minimal storage space on your device.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/prescribing.png" alt="benefit-portfolio">
                    <h4>Governance</h4>
                    <p>built into the process to enable timely but relevant updates.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/updates.png" alt="benefit-training">
                    <h4>Annual software update</h4>
                    <p>to encompass new features.</p>
                </div>
			</div>
		</section>

        <section class="appstore-buttons">
            <div class="container">
                <h3 class="mainheader">Available on:</h3>
                <img style="margin-right:20px;" src="assets/img/appstore.png"><img src="assets/img/googleplay.png">
            </div>
        </section>

        <section class="support">
            <div class="container">
                <h3 class="mainheader">Support</h3>
                <p>The administrative functions of RxGuidelines is simple and allows upload and edit of all antimicrobial guidelines into a format which is relevant to the local Trust.</p>
                <p>Local Trusts can manage the administrative function, including user rights, at different levels across the Trust. In order to comply with good governance only the Super Admin Users have the ability to publish the changes to the full application.</p>
                <p>Once the Trust guidelines have been downloaded to an end user device, this can be accessed across the hospital without the need for Wifi. Updates to the local guidelines will be automatically downloaded when the device is in range. If no internet coverage is available a warning will be presented on the device whilst an internet connection is sought, and once an internet connection has been established, the device will automatically download the latest version, ensuring the user always has the most up-to-date version to hand.</p>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/stores.png" alt="Application deployment">
                    <h4>Application deployment</h4>
                    <p>available via Google Play and the App Store.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/auto-updates.png" alt="Automatic updates">
                    <h4>Automatic updates</h4>
                    <p>pushed to the device.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/247.png" alt="24/7 availability">  
                    <h4>24/7 availability</h4>
                    <p>as a native app, once downloaded, RxGuidelines sits directly on the device.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/browsers.png" alt="Supports All Modern browsers">          
                    <h4>Supports All Modern browsers</h4>
                    <p>IE8 and above, Mozilla Firefox and Chrome.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/platforms.png" alt="Operates on Mac and Windows">
                    <h4>Operates</h4>
                    <p>on Mac and Windows PCs and is accessible via iPad.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/tutorials.png" alt="Interactive tutorials">
                    <h4>Interactive tutorials</h4>
                    <p>within the system to help with data entry.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/view.png" alt="View facility">
                    <h4>“View” facility</h4>
                    <p>to shows visuals of end user screens as Guidelines are entered.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/lock.png" alt="Lock your Guidelines">
                    <h4>Lock your Guidelines</h4>
                    <p>to avoid accidental editing/deletion.</p>
                </div>
            </div>
        </section>

    </div>

<?php include ('footer.php'); ?>
