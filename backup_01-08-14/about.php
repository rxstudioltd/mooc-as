<?php include ('header.php'); ?>

<style>
.mainnav ul .about-active a {
  background:#e74c2e !important;
  color:#fff;
  text-shadow:0 1px 1px rgba(0,0,0,0.5);
}
</style>

	<div id="body-container" class="aboutpage">
		<section class="home-intro">
            <div class="container">
                <h1>RX Guidelines</h1><h2>About us</h2>
			</div>
		</section>

        <section class="home-further">
            <div class="container">
                <p>At the centre of all Pharma Mix Group projects is the true clinical need. This can never be assumed and so Pharma Mix Group is expert in gathering clinical expertise together to fully understand the market opportunity for the project or service.</p>
                <p>We can help understand where things are going well or where improvements need to be made or a completely new strategy employed. With over 25 years of experience in the pharmaceutical industry and directly within the NHS, our Directors can ask the questions that need to be asked, impart key information on current NHS challenges and then offer real solutions to deliver projects to achieve brand objectives.</p>
                <p>&nbsp;</p>
                <p style="text-align:center; width:100%;"><img src="assets/img/rxguidelines_phones.png"></p>
                <p>&nbsp;</p>
                <p>Through our funding of forward thinking clinician projects and industry relations we have as good, if not better, insight than many competitors about how to enable patient pathways to move forward.</p>
                <p>We are committed to improving healthcare through supporting the development of innovative new services which meet the needs of the NHS. Pharma Mix Group can provide expert advice, seek industry funding and even support NHS innovators to translate their ideas into practice. These can include educational initiatives, NHS software (including mobile apps) and patient outcomes research with the possibility of the resulting IP being commercialised through appropriate channels.</p>
			</div>
		</section>

         <section class="pricing">
            <div class="container">
                <h3 class="mainheader">Pricing</h3>
                <p>All contracts will be xx months and will attract the following fees. </p>
                <img style="margin-right:20px;" src="assets/img/set-up-cost.png"><img src="assets/img/maintenance-cost.png">
                <p>&nbsp;</p>
                <p>*Annual maintenance fee will cover all bug fixes and 1 x scheduled software update.</p>
            </div>
        </section>
       
        <section class="appstore-buttons">
            <div class="container">
                <h3 class="mainheader">Available on:</h3>
                <img style="margin-right:20px;" src="assets/img/appstore.png"><img src="assets/img/googleplay.png">
            </div>
        </section>

<?php include ('footer.php'); ?>