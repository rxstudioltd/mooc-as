    <footer>
        <div class="topfoot footer-contact">
            <div class="container">
                <h3 class="mainheader">Get in touch</h3>
                <p class="subheader">If you're interested in talking to us in more detail about Rx Guidelines, give us a call.</p>
                <article class="panel footer-contact-map">
                    <div class="info">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2441.7450815598518!2d0.07831399999999501!3d52.26617399999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d876a357eb9a45%3A0x1970e136b24470aa!2sPharma+Mix+Ltd!5e0!3m2!1sen!2suk!4v1405350161918" width="500" height="250" frameborder="0" style="border:0"></iframe>
                    </div>
                </article>

                <article class="panel footer-contact-details">
                    <div class="info">
                        <section class="contactinfo">
                            <ul>
                                <li class="email first">
                                    <strong>Say hello</strong>
                                    <a href="mailto:info@pharma-mix.com" title="Email Plug and Play Franchise">info@pharma-mix.com</a>
                                </li>
                                <li class="telephone">
                                    <strong>Let's talk</strong>
                                    <a href="callto:0800 1488 592" title="Call Plug and Play Franchise">+44 (0)1223 234814</a>
                                </li>
                                <li class="address last">
                                    Unit 1, The Old Granary, Westwick, <br>Cambridge, CB24 3AR
                                </li>
                            </ul>

                            <div class="clear"></div>

                        </section>
                    </div>
                </article>
            </div>
            <a href="contact.php" class="register-interest-btn orangebtn">Click here to register your interest</a>
        </div>
        <div class="bottomfoot">
            <div class="container">
                <section class="info">
                    <p>© Pharma Mix Group Limited 2014.
                    RxGuidelines is a product from the collaboration of Pharma Mix Group and Dr Oliver Dyar. Pharma Mix Group and Dr Oliver Dyar acknowledge ITAP’s help in the creation of RxGuidelines.</p>
                </section>
            </div>
        </div>
    </footer>

    
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

</body>
</html>