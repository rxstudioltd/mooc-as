$(window).load(function(){
    $(".mainnav nav").fadeIn(500);   
});


$(document).ready(function(){
    $('.curved').arctext({radius: 1000});
    
    $('.benefits .panel, .mini-benefits a').mouseenter(function(){
        $('img', this).animo( { animation: 'tada', duration: 1 } );
    });
    $('.support .panel, .mini-benefits a').mouseenter(function(){
        $('img', this).animo( { animation: 'tada', duration: 1 } );
    });
    $('.young-creatives .subcontentone').mouseenter(function(){
        $('img', this).animo( { animation: 'bounce', duration: 2 } );
    });
	$('li:first-child').addClass('first');
	$('li:last-child').addClass('last');
    $(".mainnav ul").superfish();
	$("input[type=text], input[type=email], input[type=password], textarea").focus(function(){
		$(this).addClass("active");
		}).blur(function(){
		$(this).removeClass("active");
	});
    
    $(function() {
        var pullmain    = $('#menubtn');
            menuexpand    = $('.mainnav .mainmenu');
            menuHeight  = menuexpand.height();
        $(pullmain).on('click', function(e) {
            e.preventDefault();
            menuexpand.slideToggle();
        });
        $(window).resize(function(){
            var w = $(window).width();
            if(w > 320 && menuexpand.is(':hidden')) {
                menuexpand.removeAttr('style');
            }
        });
    });

    $('#menubtn').click(function() {
        $('#menubtn span').toggle();
    });

    $(function() {
        var pullmain    = $('#expand');
            artlist    = $('.sidebox ul');
            menuHeight  = artlist.height();
        $(pullmain).on('click', function(e) {
            e.preventDefault();
            artlist.slideToggle();
        });
        $(window).resize(function(){
            var w = $(window).width();
            if(w > 320 && artlist.is(':hidden')) {
                artlist.removeAttr('style');
            }
        });
    });

    $('#expand').click(function() {
        $('#expand span').toggle();
    });


    
});


