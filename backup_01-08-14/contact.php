<?php include ('header.php'); ?>
<style>
.mainnav ul .contact-active a {
  background:#e74c2e !important;
  color:#fff;
  text-shadow:0 1px 1px rgba(0,0,0,0.5);
}
</style>
<script>
    $(document).ready(function () {
        $('#contactform').validate({ // initialize the plugin
            rules: {
                yourname: {
                    required: true,
                    minlength: 5
                },
                jobtitle: {
                    required: true,
                    minlength: 5
                },
                youremail: {
                    required: true,
                    email: true
                },
                yourtelephone: {
                    required: true,
                    minlength: 5
                },
                youraddress: {
                    required: true,
                    minlength: 5
                },
                message: {
                    required: true,
                    minlength: 5
                }
            },
            submitHandler: function (form) {
                form.submit();
                return false;
            }
        });
    });
</script>

	<div id="body-container" class="aboutpage">

        <div class="registerbox">
            <div class="container">
	   		<h3>Next steps - Register your interest</h3>
	   		<p>Give us a call today on <strong>+44 (0)1223 234814</strong> to discuss the offering in more detail or complete the form below to register your details and we'll be in touch with you.</p>
		   	<div class="contactform registerform">
		   		<div class="">
                    <form id="contactform" method="post" action="thanks.php">
                        <ul>
                            <li class="first">
                                <label>Your name</label>
                                <span class="your-name">
                                    <input type="text" name="yourname" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>

                            <li>
                                <label>Job Title</label>
                                <span class="your-email">
                                    <input type="text" name="jobtitle" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>

                        
                            <li>
                                <label>Your email address</label>
                                <span class="your-email">
                                    <input type="email" name="youremail" value="" size="40" class="uniform-input email error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>
                        
                            <li>
                                <label>Telephone number</label>
                                <span class="your-telephone">
                                    <input type="text" id="yourtelephone" name="yourtelephone" value="" size="40" class="uniform-input text error" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>
                                                    
                            <li>
                                <label>Address</label>
                                <span class="your-address">
                                    <textarea name="youraddress" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                            <li>
                                <label>Comments</label>
                                <span class="your-address">
                                    <textarea name="message" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                        </ul>
                        
                        <input type="submit" value="Send">
                        
                    </form>
                </div>
            </div>
        </div>
        </div>			
        <div class="clear"></div>
    </div>

<?php include ('footer.php'); ?>