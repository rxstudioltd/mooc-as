<?php include ('header.php'); ?>

<style>
.mainnav ul .news-active a {
  background:#e74c2e !important;
  color:#fff;
  text-shadow:0 1px 1px rgba(0,0,0,0.5);
}
</style>

	<div id="body-container" class="aboutpage">
		<section class="home-intro">
            <div class="container">
                <h1>RX Guidelines</h1><h2>News</h2>
			</div>
		</section>

        <section class="news">
            <div class="container">
                <h3 class="mainheader">15 Pilot sites adopt RX Guidelines</h3>
                <img style="margin-right:20px;" src="assets/img/nhs.png"><img src="assets/img/welsh_nhs.png">
                <p>&nbsp;</p>
                <p>Aneurin Bevan University Health Board</p>
                <p>North Devon District Trust</p>
                <p>Central Manchester Foundation NHS Trust</p>
                <p>Derby Hospitals Foundation NHS Trust</p>
                <p>Royal Devon and Exeter NHS Trust</p>
                <p>Hywel Dda University Health Board</p>
                <p>North Bristol NHS Trust</p>
                <p>Plymouth Hospitals NHS Trust</p>
                <p>Nottingham University Hospitals NHS Trust</p>
                <p>Abertawe Bro Morgannwg University Health Board</p>
                <p>Taunton and Somerset NHS Trust </p>
                <p>Yeovil District Hospital NHS Foundation Trust</p>
                <p>University Hospitals of Leicester, NHS Trust</p>
                <p>Hinchingbrooke NHS Trust</p>
                <p>Weston Area Health NHS Trust</p>
			</div>
		</section>

        <section class="appstore-buttons">
            <div class="container">
                <h3 class="mainheader">Available on:</h3>
                <img style="margin-right:20px;" src="assets/img/appstore.png"><img src="assets/img/googleplay.png">
            </div>
        </section>

 <?php include ('footer.php'); ?>