<?php include ('header.php'); ?>

	<div id="body-container">
		<section class="home-intro">
            <div class="container">
                <h1>RX Guidelines</h1>
                <h2>Antibiotic stewardship at your fingertips</h2> 
                <h1>Mobile access to local antimicrobial prescribing guidelines via iphone and android devices.</h1>
			</div>
		</section>

        <section class="home-further">
            <div class="container">
                <p style="text-align:center; width:100%;"><img src="assets/img/rxguidelines_phones.png"></p>
            </div>
        </section>

        <section class="pricing" id="about">
            <div class="container">
                <h3 class="mainheader">About</h3>
                    <p>RxGuidelines seeks to improve the ease with which antimicrobial guidelines are accessible to healthcare professionals in local Trusts and enable good governance across the organisation. 
                    Developed by an expert faculty across Europe this application will be an essential tool for education and support of junior doctors within local Trusts to enable compliance to local guidelines with respect to antibiotic choice, length of treatment and other governance recommendations.
                    The application is available across iOS and Android platforms giving accessibility and usage to a wide number of devices.
                    “There is high level of smartphone ownership and usage among medical students and junior doctors. Both groups endorse the development of more apps to support their education and clinical practice” <sup>1</sup>.
                    <p>&nbsp;</p>
                    <div class="reference-text"><small>References<br>1. Smartphone and medical related App use among medical students and junior doctors in the United Kingdom (UK): a regional survey. Karl Frederick Braekkan Payne<sup>1</sup>, Heather Wharrad<sup>2*</sup> and Kim Watts<sup>2</sup> BMC Medical Informatics and decision making</small></div>
            </div>
        </section>

        <section class="benefits"  id="features">
            <div class="container">
                <h3>Key features</h3>
                <div class="panel">
                    <img width="160" height="160" src="assets/img/ease_of_use.png" alt="benefit-leads">
                    <h4>Personalise your App</h4>
                    <p>list your most used Guidelines by order of preference. </p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/faculty.png" alt="benefit-marketing">
                    <h4>Faculty led product</h4>
                    <p>TBC</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/encyclopaedia.png" alt="benefit-support">  
                    <h4>Designed by clinicians</h4>
                    <p>TBC</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/prescribing.png" alt="benefit-portfolio">
                    <h4>Governance</h4>
                    <p>built into the process to enable timely but relevant updates.</p>
                </div>
			</div>
		</section>

        <section class="appstore-buttons" id="pricing">
            <div class="container">
                <h3 class="mainheader">Pricing</h3>
                <p>All contracts will be xx months and will attract the following fees. </p>
                <img class="pricing-first" src="assets/img/set-up-cost.png"><img src="assets/img/maintenance-cost.png">
                <p>&nbsp;</p>
                <p>*Annual maintenance fee will cover all bug fixes and 1 x scheduled software update.</p>
                <p>&nbsp;</p>
                <h3 class="mainheader">Available on:</h3>
                <img class="pricing-first" src="assets/img/appstore.png"><img src="assets/img/googleplay.png">
            </div>
        </section>

        <section class="support" id="faq">
            <div class="container">
                <h3 class="mainheader">FAQs</h3>
                <div class="panel">
                    <img width="160" height="160" src="assets/img/stores.png" alt="Application deployment">
                    <h4>How do I access the App?</h4>
                    <p>RxGuidelines is available via Google Play and the App Store.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/analytics.png" alt="Memory">
                    <h4>Will this take up a lot of memory on my iPhone/Android device?</h4>
                    <p>No, the App has been optimised to take up minimal storage space on your device.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/247.png" alt="24/7 availability">  
                    <h4>Do I need internet access to use the App?</h4>
                    <p>No, once you have downloaded your Trust’s guidelines they will remain on your App for 24/7 availability.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/browsers.png" alt="Supports All Modern browsers">          
                    <h4>What browsers are supported by RxGuidelines?</h4>
                    <p>RxGuidelines supports All Modern browsers (including IE8 and above, Mozilla Firefox and Chrome).</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/platforms.png" alt="Operates on Mac and Windows">
                    <h4>Can I only access the App through my iPhone/Android device?</h4>
                    <p>RxGuidelines also operates on Mac and Windows PCs and is accessible via iPad.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/tutorials.png" alt="Interactive tutorials">
                    <h4>Can I personalise my App?</h4>
                    <p>Yes, you can list your most used Guidelines by order of preference. Your Trust name will also appear on each page.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/updates.png" alt="Automatic Updates">
                    <h4>How can I be sure I am using the most recent Guidelines for my Trust?</h4>
                    <p>Published guideline changes are automatically pushed to your device when an internet connection is available.</p>
                </div>

                <div class="panel">
                    <img width="160" height="160" src="assets/img/lock.png" alt="Lock your Guidelines">
                    <h4>How do I control changes to my Guidelines?</h4>
                    <p>Permission settings are in place to ensure all requested changes are channelled through relevant Super Users. You can also “lock” Guidelines which provides additional security.</p>
                </div>
            </div>
        </section>

    </div>

<?php include ('footer.php'); ?>
