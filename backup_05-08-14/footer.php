<script>
    $(document).ready(function () {
        $('#contactform').validate({ // initialize the plugin
            rules: {
                yourname: {
                    required: true,
                    minlength: 5
                },
                jobtitle: {
                    required: true,
                    minlength: 5
                },
                youremail: {
                    required: true,
                    email: true
                },
                yourtelephone: {
                    required: true,
                    minlength: 5
                },
                youraddress: {
                    required: true,
                    minlength: 5
                },
                message: {
                    required: true,
                    minlength: 5
                }
            },
            submitHandler: function (form) {
                form.submit();
                return false;
            }
        });
    });
</script>

    <footer id="contact">
        <div class="registerbox">
            <div class="container">
            <h3>Next steps - Register your interest</h3>
            <p>Register your details and we'll be in touch with you.</p>
            <div class="contactform registerform">
                <div class="">
                    <form id="contactform" method="post" action="thanks.php">
                        <ul>
                            <li class="first">
                                <label>Your name</label>
                                <span class="your-name">
                                    <input type="text" name="yourname" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>

                            <li>
                                <label>Job Title</label>
                                <span class="your-email">
                                    <input type="text" name="jobtitle" value="" size="40" class="uniform-input text error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>

                        
                            <li>
                                <label>Your email address</label>
                                <span class="your-email">
                                    <input type="email" name="youremail" value="" size="40" class="uniform-input email error" minlength="2" required="" aria-required="true" aria-invalid="true">
                                </span> 
                            </li>
                        
                            <li>
                                <label>Telephone number</label>
                                <span class="your-telephone">
                                    <input type="text" id="yourtelephone" name="yourtelephone" value="" size="40" class="uniform-input text error" required="" aria-required="true" aria-invalid="true">
                                </span>
                            </li>
                                                    
                            <li>
                                <label>Address</label>
                                <span class="your-address">
                                    <textarea name="youraddress" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                            <li>
                                <label>Comments</label>
                                <span class="your-address">
                                    <textarea name="message" cols="40" rows="10" required="" aria-required="true" class="error"></textarea>
                                </span>
                            </li>

                        </ul>
                        
                        <input type="submit" value="Send">
                        
                    </form>
                </div>
            </div>
        </div>
        
        </div>
        <div class="bottomfoot">
            <div class="container">
                <section class="info">
                    <strong>Let's talk</strong>
                    <h3 class="mainheader">+44 (0)1223 234814</h3>
                    <strong>Email</strong>
                    <h3 class="mainheader">info@pharma-mix.com</h3>
                    <p>© Pharma Mix Group Limited 2014.
                    RxGuidelines is a product from the collaboration of Pharma Mix Group and Dr Oliver Dyar. Pharma Mix Group and Dr Oliver Dyar acknowledge ITAP’s help in the creation of RxGuidelines.</p>
                </section>
            </div>
        </div>
    </footer>

    <div id="back-top">
        <a href="#top"><span></span>Top</a>
    </div>

    
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $("#back-top").click(function(){
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    </script>

</body>
</html>