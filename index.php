<?php include ('header.php'); ?>

	<div id="body-container">
		<section class="home-intro">
            <div class="flash-free"><a href="MOOC_AS_Call_for_funding.pdf"><img src="assets/img/flash-download.png" alt="Free for the first 6 months with no obligation"></a></div>
            <div class="container">
                <div style="text-align:center; width:100%;"><h3 style="font-size:28px;">Call for Funding</h3></div>
                <h2>Massive Open Online Course for Antimicrobial Stewardship</h2> 
			</div>
            <section class="benefits">
                <div class="panel-holder">
                    <div class="panel">
                        <img width="80" height="80" src="assets/img/education.png" alt="benefit-support">  
                        <h4>Education and empowerment of health care professionals</h4>
                    </div>

                    <div class="panel">
                        <img width="80" height="80" src="assets/img/health_globally.png" alt="benefit-marketing">
                        <h4>Enhancing health<br>globally</h4>
                    </div>

                    <div class="panel">
                        <img width="80" height="80" src="assets/img/infection_prevention.png" alt="benefit-portfolio">
                        <h4>Effective infection prevention and management</h4>
                    </div>
                </div>
            </section>
		</section>

        <section class="pricing" id="about">
            <div class="container">
                <h3 class="mainheader">About the Project</h3>
                <p>The mission of a <a href="MOOC_AS_Call_for_funding.pdf">MOOC for Antimicrobial Stewardship</a> is to support the fight against antimicrobial resistance [AMR] through: Education and empowerment of health care professionals so as to provide timely, high quality and safe infection management across all healthcare communities that will enhance the health of the populations they serve.</p>
                <p>The MOOC will be developed through a multi-stakeholder collaborative incremental approach met by the following principles:</p>

                <section class="how">
                    <div class="panel-holder">
                        <div class="panel">
                            <h4>Fit for diverse learning needs of the differing global teams of healthcare professionals and differing global healthcare systems</h4>
                        </div>
                        <div class="panel">
                            <h4>Focus on implementation of stewardship with evaluation and engagement through measurement, feedback and action</h4>
                        </div>
                        <div class="panel">
                            <h4>An interactive and blended approach that will aim to meet the diverse learning of healthcare professionals in different healthcare systems</h4>
                        </div>
                        <div class="panel">
                            <h4>Evaluation of value</h4>
                        </div>
                        <div class="panel">
                            <h4>A blended approach to learning, including supplementary bespoke community specific resources</h4>
                        </div>
                        <div class="panel">
                            <h4>Provision of core and advance competency learning</h4>
                        </div>
                    </div>
                </section>
            </div>
        </section>

        <section class="home-further" id="faculty">
            <div class="container">
                <h3 class="mainheader">Faculty</h3>

                <section class="faculty">
                    <h4 class="mainheader">Lead / host organisations</h4>

                    <section class="available">
                        <div class="panel-holder">
                            <div class="panel">
                                <p>Lead professional organisation:<br>
                                <img style="margin-top:20px;" src="assets/img/bsac-logo.png" alt="BSAC logo"></p>
                            </div>
                            <div class="panel">
                                <p>Lead academic institution:<br>
                                <img src="assets/img/dundee-logo.jpg" alt="University of Dundee logo"></p>
                            </div>
                        </div>
                    </section>
                    
                    <p>• Commissioning and licensing of the technical infrastructure and capacity for hosting the MOOC<br>
                    • Commissioning, development, quality assurance, peer review and ongoing update of content<br>
                    • Managing promotion to, registration by and support of students<br>
                    • Identifying and implementing modes and methods of course evaluation, accreditation and certification</p>
                </section>
            </div>
        </section>

        <section class="home-partners" id="partners">
            <div class="container">
                <h3 class="mainheader">Partners</h3>
                <p>To develop the MOOC we are building a coalition of clinical, academic, industry ,charities &amp; governmental partners to take forward our ambition to deliver an innovative, high quality, learner centered on line course that is sustainable and flexible to the need of the healthcare professional.  Our discussions to date with many such organisations globally from Australia, Africa, India , middle east and Europe  have unanimously supported an urgent need for this.</p>
                
                <section class="partners">
                    <section class="works">
                        <div class="panel-holder">
                            <div class="panel">
                                <div class="panel-title-header"><h4>Philanthropic organisations</h4></div>
                                <p><img src="assets/img/bullet-icon.png"><br>Financial support for MOOC development and other collaboration as offered</p>
                            </div>
                            <div class="panel">
                                <div class="panel-title-header"><h4>National federal and learned organisations</h4></div>
                                <p><img src="assets/img/bullet-icon.png"><br>Financial support for licensing, maintenance and development arrangements</p>
                                <p><img src="assets/img/bullet-icon.png"><br>Dissemination of the MOOC within national localities</p>
                            </div>
                            <div class="panel">
                                <div class="panel-title-header"><h4>Lead national academic institutions</h4></div>
                                <p><img src="assets/img/bullet-icon.png"><br>Content generation, peer review and update</p>
                                <p><img src="assets/img/bullet-icon.png"><br>Assistance in securing student enrolment</p>
                                <p><img src="assets/img/bullet-icon.png"><br>Promotion</p>
                            </div>
                            <div class="panel">
                                <div class="panel-title-header"><h4>Commercial partners – Pharmaceutical industry, diagnostics, device companies</h4></div>
                                <p><img src="assets/img/bullet-icon.png"><br>Financial support to seed fund development of the MOOC</p>
                                <p><img src="assets/img/bullet-icon.png"><br>Financial support to content development</p>
                                <p><img src="assets/img/bullet-icon.png"><br>Educational resources for peer review as part of the MOOC</p>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </section>

        <section class="home-features" id="news">
            <div class="timeline-holder">
                <h3 class="mainheader">Latest News</h3>
                <div class="timeline two-col">
                    <div data-page="1" class="format-standard hentry dwtl normal dwtl-left" style="opacity: 1;">
                        <div class="entry-inner">
                            <h3 class="smallheader">Fighting antimicrobial resistance through education</h3>
                            <p>That antibiotic resistance is a problem of global importance is beyond doubt.  January 2013 saw the World Economic Forum place antibiotic resistance on the global risk register alongside terrorism and global warming. The antibiotic pipeline is near empty and although there is research, regulatory, clinical and political moves to regenerate antibacterial drug discovery and development it will be many years if not decades before these efforts bear results.</p>
                            <p>Conservation of effective antimicrobial agents is essential, and a range of global and regional authorities such as the World Health Organisation have identified antimicrobial stewardship as one of the key interventions in combating antimicrobial resistance. This global leadership is supported by international activity, such as the development of the Chennai declaration and establishment of initiatives such as Antibiotic Action and WAAR.</p>
                            <p>Acknowledging the critical role antimicrobial stewardship will play needs to be translated into action, action that is relevant across the differing health structures globally and action that can be easily understood, translated, implemented and measured.</p>
                            <p>Healthcare professionals largely work within multi-professional teams, where stewardship skills to develop, implement and measure the impact of interventions are relatively scarce as is the existing infrastructure and resource to support them. Therefore, it is essential that these professionals develop these skills so as to lever change and implement stewardship across a range of health care settings, systems, geographies and cultures. Educational courses aim to support these ambitions.</p>
                            <p>A collaboration of experts from different professional societies across the globe with experience and skills in providing this educational resource is an important first step in spreading knowledge about effective stewardship. The training faculty aims to reflect the diversity of experience and skills in this area with a view to address generic and local training needs. 
</p>
                        </div>
                        <div class="entry-inner">
                            <h3 class="smallheader">What is a MOOC?</h3>
                            <p>Massive Open On-line Courses (MOOCs) are web-based, open-access learning modules that utilize multiple resources and learning media including videos, case studies, electronic textbooks and interactive user forums.    MOOCs offer university standard courses without the requirement to complete an entire programme, enabling users to access training of direct relevance to them at any given point in time.
                            <p>The principle of a MOOC is that module content is free at the point of enrolment with some models offering enhanced tutorship, examinations and certification for which a fee is charged.</p>
                            <p>Having emerged from the US academic system in the late 2000, since when a number of host platform providers have entered the market including edX, Coursera, EduKart, UDACITY and FutureLearn demonstrating commercial confidence in the MOOC arena.  In 2012 the Open University launched FutureLearn, partnering with more than 20 UK and international universities and other institutions such as the British Council, the British Library and the British Museum.</p>
                            <p>The University of Edinburgh became the first UK academic institution to offer MOOC in January 2013.  Six courses were launched which attracted 308,000 students to enrol.     MOOCs are currently offered by UK universities including: Birmingham, Leeds, Leicester, Nottingham, Southampton and Warwick.  The BSAC/University of Dundee international MOOC will be the first on antimicrobial stewardship to be offered outside of the United States.
</p>
                        </div>
                    </div>
                    <div data-page="1" class="format-standard hentry dwtl normal dwtl-right" style="opacity: 1;">
                        <div class="entry-inner">
                            <h3 class="smallheader">Call for consortia partners</h3>
                            <p>A baseline seed-fund target of £375,000 is being sought to develop, test, licence and launch an open access Antimicrobial Stewardship MOOC.  Contributions from stakeholders are proposed as follows:</p>
                            <p>STAKEHOLDER: Philanthropic foundations / trusts<br>
                                SEED-FUNDING TARGET (non-recurring educational grant): £50,000- £100,000<br>
                                TARGET NUMBER (minimum target number needed): 1</p>
                            <p>STAKEHOLDER: Pharmaceutical companies<br>
                                SEED-FUNDING TARGET (non-recurring educational grant): £30,000- £75,000<br>
                                TARGET NUMBER (minimum target number needed): 7</p>
                            <p>STAKEHOLDER: Academic institutions<br>
                                SEED-FUNDING TARGET (non-recurring educational grant): £30,000- £50,000<br>
                                TARGET NUMBER (minimum target number needed): 3</p>
                            <p>STAKEHOLDER: Biotech / SME<br>
                                SEED-FUNDING TARGET (non-recurring educational grant): £15,000- £30,000<br>
                                TARGET NUMBER (minimum target number needed): 2</p>
                            <p>A separate business case to identify a sustainable funding stream for maintaining, developing and supporting this and likewise MOOC courses is under development.  Support from consortia partners can be via non-recurring educational grants or continued contributions to sustain development of content going forward.</p>
                            <p>To express your interest in becoming a consortia partner contact the Programme Director, Professor Dilip Nathwani or BSAC, Chief Executive Officer, Tracey Guise – tguise@bsac.org.uk | +44 (0) 121 236 1988
</p>
                        </div>
                        <div class="entry-inner">
                            <h3 class="smallheader">Setting sail to develop the Antimicrobial Stewardship MOOC!</h3>
                            <p>BSAC officers met with colleagues from academia and industry on a balmy evening in Barcelona during the ECCMID conference week.  Having failed to secure a regular venue courtesy of the grand prix, we boarded a turquish schooner and took to the open seas!  We shared our vision of how the Society wishes to extend its expertise and educational activities in a bid to address antimicrobial resistance.</p>
                            <p>BSAC has ambitions to develop, alongside a range of international partners, a Massive Open Online Course (MOOC) on Antimicrobial Stewardship.  The course will offer healthcare professionals the world over open access to an eclectic accredited training course that can be used as part of formal education, strategically to plan ahead and reactively to address in situ clinical situations.  We aim to have the MOOC launched by early Summer 2015</p>
                            <p>Two months later and we are in negotiation with a UK University of international renown, in discussion with a major international online learning platform provider regards development of the MOOC, are talking to a range of stakeholders from industry and have engaged with professional organisations as far afield as Australia.</p>
                            <p>The first stage will be to identify adequate seed funding to get our MOOC launched, and we are pleased to report that our necessarily ambitious plans are beginning to come together.</p>
                            <p>The second stage will be to identify faculty able to assist in the development, peer review and quality assurance of material for the MOOC.  At this point in time we are seeking expressions of interest from individuals willing to share their expertise and help us to develop a truly life-saving and life-enhancing educational resource.</p>
                            <p>To register your expression of interest as a member of faculty or stakeholder sponsor please contact:<br>Tracey Guise, BSAC CEO</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

<?php include ('footer.php'); ?>
