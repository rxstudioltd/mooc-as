<?php header( "refresh:5;url=http://antimicrobial-stewardship-mooc.com/" ); ?>
<?php
set_include_path('./');

$headers  = "MIME-Version: 1.0\r\n"; 
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

$contact_name 	= $_POST['yourname'];
$contact_jobtitle   = $_POST['jobtitle'];
$contact_email 	= $_POST['youremail'];
$contact_number   = $_POST['yourtelephone'];
$contact_address   = $_POST['youraddress'];
$contact_message 	= $_POST['message'];

$sitename		= "antimicrobial-stewardship-mooc.com/";


$to 			= "TGuise@bsac.org.uk";

$date 			= date("m/d/Y H:i:s");
$ToSubject 		= "From $contact_name via $sitename";
$comments 		= $msgPost;
$EmailBody 		= "A visitor to $sitename has sent the following<br /><br />
              	<b>Sent By:</b>
				<br />
				$contact_name
			 	<br /><br />
                <b>Job Title:</b>
                <br />
                $contact_jobtitle
                <br /><br />
				<b>Email:</b>
				<br />
				$contact_email
			 	<br /><br />
                <b>Phone Number:</b>
                <br />
                $contact_number
                <br /><br />
                <b>Address:</b>
                <br />
                $contact_address
                <br /><br />
				<b>Comments</b>
			  	<br />
				$contact_message
				<br /><br />";  
$EmailFooter	= "<b>Sent:</b> $date<br /><br />";
$Message 		= $EmailBody.$EmailFooter;
$ok = mail($to, $ToSubject, $Message, $headers . "From:$contact_name <".$to.">");
if($ok){
	echo "retval=1";
}else{
	echo "retval=0";
}
?>

<?php include ('header.php'); ?>

<style>
.mainnav ul .home-active a {
  background:#e74c2e !important;
  color:#fff;
  text-shadow:0 1px 1px rgba(0,0,0,0.5);
}
</style>

<div id="body-container">
    <section class="home-intro">
        <div class="container">
            <h2>Thanks for your enquiry</h2> 
            <h1>We will be in touch soon.</h1>
            <p>&nbsp;</p>
            <h1>This page will automatically redirect to the homepage. If you do not get redirected <a href="/">click here</a></h1>
        </div>
    </section>
</div>

 <footer>
    <div class="bottomfoot">
            <div class="container">
                <section class="info">
                    <p><img style="margin-top:20px;" src="assets/img/bsac-logo-white-text.png" alt="BSAC logo"><br>
                    ADDRESS: 53 Regent Place | Birmingham | B1 3NJ</p>
                    <p>© BSAC 2014.</p>
                </section>
            </div>
        </div>
</footer>


    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $("#back-top").click(function(){
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });
    </script>
</body>
</html>

